$('#besede').load('swearWords.txt');
function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  var arrayVnos = sporocilo.split(' ');
  
  var besedeVn = $('#besede').text();
  var arraySwears = besedeVn.split('\n');
  var znak = '';
 
  
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.presirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

      var re;
    for(var i=0; i<arrayVnos.length; i++){
      znak1 = '';
      var arrayCrke = arrayVnos[i].split(''); 
       for(var u=0; u<arrayCrke.length; u++){
          if(arrayCrke[u]=='.' || arrayCrke[u] == '?' || arrayCrke[u] == '!' || arrayCrke[u] == ','){
            znak1 = arrayCrke[u];
            arrayVnos[i] = arrayVnos[i].replace(znak1, ''); 
            
          }
        }
        for(var j=0; j<arraySwears.length; j++){
          if(arrayVnos[i] == arraySwears[j]){
            var arrShr = new Array(arrayVnos[i].length);
            for(var k=0; k<arrShr.length; k++){
              arrShr[k]='*';
            }
            arrayVnos[i] = arrayVnos[i].replace(arrayVnos[i], arrShr);
            for(var l=0; l<arrayVnos[i].length; l++){
              arrayVnos[i] = arrayVnos[i].replace(',','');
            }
           
            break;
          }
        }
        arrayVnos[i] += znak1;
    }
    
    sporocilo = arrayVnos.toString();
    for(var i=0; i<sporocilo.length; i++){
      sporocilo = sporocilo.replace(',', ' ');
    }
      sporocilo = sporocilo.replace(/;\)/g, '&ltimg src ="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/&gt');
      sporocilo = sporocilo.replace(/:\)/g, '&ltimg src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/&gt');
      sporocilo = sporocilo.replace(/\(y\)/g, '&ltimg src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/&gt');
      sporocilo = sporocilo.replace(/:\*/g, '&ltimg src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/&gt');
      sporocilo = sporocilo.replace(/:\(/g, '&ltimg src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/&gt');
      
      var kanal = $('#kanal').text();
      var znak1;
      for(var i in kanal){
        if(kanal.charAt(i)=='@'){
          kanal = kanal.substring((i+2), kanal.length);
        }
      }
      
      klepetApp.posljiSporocilo(kanal, sporocilo);
      if(sporocilo.charAt(0) == '<' && sporocilo.charAt(sporocilo.length-1) == '>'){
         $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      }else{
         for(var i=0; i<sporocilo.length; i++){
          sporocilo = sporocilo.replace('&lt', '<');
          sporocilo = sporocilo.replace('&gt', '>');
         }
         $('#sporocila').append($('<div style="font-weight: bold"></div>').html(sporocilo));
      }
    

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));

  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.'
       $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {

   var st = 0;
   // var novElement= $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
   for (var j = 0; j < sporocilo.besedilo.length; j++) {
     if(sporocilo.besedilo.charAt(j) == '<' || sporocilo.besedilo.charAt(j) == '>'){
        
        var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
        $('#sporocila').append(novElement);
        st = 1;
         break;
    }
   }
    if(st == 0){
       for(var i=0; i<sporocilo.besedilo.length; i++){
         sporocilo.besedilo = sporocilo.besedilo.replace('&lt', '<');
         sporocilo.besedilo = sporocilo.besedilo.replace('&gt', '>');
       }
       var novElement1 = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);//$('#sporocila').append($('<div style="font-weight: bold"></div>').html(sporocilo));
       $('#sporocila').append(novElement1);
       
    }

  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  socket.on('uporabniki', function(uporabniki){
   $('#seznam-uporabnikov').empty();
    for(var i in uporabniki.uporabniki){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki.uporabniki[i]));
    }
    
  });
  
  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  setInterval(function(){
    socket.emit('posodabljanje');
  },1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});